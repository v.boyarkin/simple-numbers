import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.concurrent.CopyOnWriteArraySet;

public class Main {

    private static CopyOnWriteArraySet<Integer> simpls = new CopyOnWriteArraySet<Integer>();

    public static void main(String ... args) {
        int totalNums = Integer.parseInt(args[0]);
        int amountThreads = Integer.parseInt(args[1]);
        int chunkSize = Double.valueOf(totalNums/amountThreads).intValue();
        List<Thread> threads = new ArrayList<Thread>(amountThreads);
        for (int i = 0; i < amountThreads; i++) {
            Thread thread = startThread(chunkSize * i + 1, chunkSize * (i + 1));
            thread.start();
            threads.add(thread);
        }
        boolean finished;
        do {
            finished = true;
            for (Thread thread : threads) {
                if (thread.isAlive()) {
                    finished = false;
                    break;
                }
            }
        } while(!finished);
        System.out.println(simpls);
    }

    private static Thread startThread(final int begin, final int end) {
        Thread thread = new Thread(new Runnable() {
            public void run() {
                System.out.println("thread start: " + Thread.currentThread().getName());
                for (int i = begin; i < end; i++) {
                    boolean next = false;
                    for (int j = 2; j < i; j++) {
                        if (i % j == 0 && i != j) {
                            next = true;
                            break;
                        }
                    }
                    if (next) {
                        continue;
                    } else {
                        simpls.add(i);
                    }
                }
                System.out.println("thread "+ Thread.currentThread().getName() +" begin=" + begin + " end=" + end + " finished");
            }
        });
        return thread;
    }
}
